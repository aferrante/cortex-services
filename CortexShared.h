// CortexShared.h: interface for the CCortexMessage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CORTEXSHARED_H__BF3CE24A_470B_4F0F_8D0E_A7B654EECA03__INCLUDED_)
#define AFX_CORTEXSHARED_H__BF3CE24A_470B_4F0F_8D0E_A7B654EECA03__INCLUDED_

#include <sys/timeb.h>
#include "CortexDefines.h"
#include "../Common/KEY/LicenseKey.h"
#include "../Common/TXT/BufferUtil.h"
#include "../Common/LAN/NetUtil.h" 


// XML nodes for settings comm to DLLs.
/* this is one setting node sample
<setting>
	<category>network</category>
	<category_display>Network Settings</category_display>
	<parameter>port</parameter>
	<parameter_display>TCPIP port</parameter_display>
	<value>12345</value>
	<desc>The TCPIP port for the main communication	listener</desc>
	<pattern>Integer</pattern>
	<min_len>null</min_len>
	<max_len>null</max_len>
	<min_value>1024</min_value>
	<max_value>65535</max_value>
	<sysonly>true</sysonly>
</setting>
*/
#define DLL_SETTING_XML_CATEGORY						0
#define DLL_SETTING_XML_CATEGORY_DISPLAY		1
#define DLL_SETTING_XML_PARAM								2
#define DLL_SETTING_XML_PARAM_DISPLAY				3
#define DLL_SETTING_XML_VALUE								4
#define DLL_SETTING_XML_DESC								5
#define DLL_SETTING_XML_PATTERN							6
#define DLL_SETTING_XML_MIN_LEN							7
#define DLL_SETTING_XML_MAX_LEN							8
#define DLL_SETTING_XML_MIN_VAL							9
#define DLL_SETTING_XML_MAX_VAL							10
#define DLL_SETTING_XML_SYSONLY							11

#define DLL_SETTING_XML_NODECOUNT						12


// Generic Plug-in function prototype
typedef int (CALLBACK* LPFNDLLCTRL)(void**, UINT); //DLLCtrl
#define DLLFUNC_NAME "DLLCtrl"

// Generic callbacks for DLLs to send things to the modules Message and As-Run tables.
typedef int (__stdcall* LPFNMSG)( int, char*, char* );
typedef int (__stdcall* LPFNAR)( int, char*, char*, char*, int);
// Generic callbacks for DLLs to send status message to module info interface.
typedef int (__stdcall* LPFNSTATUS)( int, int, char*);

#define SETSTATUS_MESSAGE_MAXLEN 256 // maximum length of the DLL status message

#define DLL_TYPE_CONTROL				 0  // control module
#define DLL_TYPE_INTERPRETER     1  // interpreter module
#define DLL_TYPE_STYLE			     2  // style module
#define DLL_TYPE_CLIENT			     3  // client module
#define DLL_TYPE_TAB_PLUGIN			 4  // Tabulator plugin module


#define DLL_FLAG_DISPATCH     0x00010000 // allowed to get the dispatcher callback
#define DLL_FLAG_MSG			    0x00020000 // allowed to get the UI message callback
#define DLL_FLAG_ASRUN        0x00040000 // allowed to get the UI asrun callback
#define DLL_FLAG_STATUS		    0x00080000 // allowed to get the UI set status callback

#define DLLCMD_INIT						0x0010 // A signal to the DLL that it should run any internal initialization routines. The DLL may want to wait for settings to be set before proceeding to context-sensitive routines.
#define DLLCMD_SET_DISPATCH		0x0011 // Sets a callback function address so that the DLL may send messages to the calling application's message dispatch procedure, which writes log lines, sends emails out, etc depending on severity level and other criteria.
#define DLLCMD_SET_MSG				0x0012 // Sets a callback function address so that the DLL may send messages to the calling application's web user interface for messages.
#define DLLCMD_SET_ASRUN			0x0013 // Sets a callback function address so that the DLL may send messages to the calling application's web user interface for as-run items.
#define DLLCMD_GET_SETTINGS		0x0014 // Obtain a list of settings that the DLL would like to make sure exists in the user interface, for use inside the DLL.
#define DLLCMD_SET_SETTINGS		0x0015 // Set a list of settings with values from the web interface (database).
#define DLLCMD_SET_STATUS			0x0016 // Sets a callback function address so that the DLL may send status updates to the calling application's web user interface for module status.

#define DLLCMD_DEINIT					0x0020 // A signal to the DLL that it should run any internal de-initialization routines. After the DEINIT command is received the DLL should prepare to finalize any messaging back to the calling application.
#define DLLCMD_MSGEND					0x0021 // A signal to the DLL that it should not send any further messages through any messaging callbacks, including as-run.
#define DLLCMD_QUERY					0x0022 // A query to the DLL to ask it if it has finished deinitialization.  This command will repeat until a succesful return, or a de-init timeout has elapsed.
#define DLLCMD_RELEASE				0x002e // A signal to the DLL that the initialization routine is complete.
#define DLLCMD_DLLEND					0x002f // A signal to the DLL that it is about to be unloaded.

#define DLLCMD_FREE_BUFFER		0x00ff // A call to free a buffer.  This will be called to free buffers that have been allocated by the calls specified above. Note that this is a different call from the call to free an object, regardless of whether the DLL uses the same allocation/deallocation functions in the call.  That command is given here for completeness, but is not used in initialization
#define DLLCMD_FREE_OBJECT		0x00fe // A call to free an object.


//LPFNMSG SendMsg( int nType, char* pszSender, char* pszMessage );
//LPFNAR  SendAsRunMsg( int nType, char* pszSender, char* pszMessage, char* pszEventID, int nChannelID );
//LPFNSTATUS SetControlModuleStatus( int nModuleID, int nType, char* pszStatus );
//LPFNSTATUS SetInterpreterModuleStatus( int nModuleID, int nType, char* pszStatus );


// defined in Messager
//LPFNDM  DispatchMessage( unsigned long ulFlags, char* pszDestination, char* pszCaller, char* pszMessage );



#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCortexMessage  
{
public:
	CCortexMessage();
	virtual ~CCortexMessage();

	char* m_pchResponse[CX_XML_BUFFER_COUNT];  // various XML element buffers
	int m_nTxStep;  // Transmission step (so we can detect where in the process a failure might occur)
	int m_nFailure; // failure code
	int m_nType;  // command type
	char* m_pchType;

	int FormatContent(int nBufferIndex, int nBufferMax, char* pchContent, ...);
	int SetContent(int nBufferIndex, char* pchContent);
	int ClearContent(int nBufferIndex);
	char* XMLTextNodeValue(IXMLDOMNodePtr pNode);
	int SetType(char* pchContent);
};


class CCortexUtil  
{
public:
	CCortexUtil();
	virtual ~CCortexUtil();

	char* ConvertWideToA(void* pbstrIn);
	int ConvertWideToA(char* pchBuffer, unsigned long ulMaxBufLen, void* pbstrIn);
};


// can be used for debug logging bits
// can be used for status bits
// can be used for enumerated types of any kind, such as active threads
class CCortexItemBit  
{
public:
	CCortexItemBit();
	virtual ~CCortexItemBit();

	unsigned __int64 m_ui64Value;
	char* m_szName;
	char* m_szDescription;
	void* m_pVoid;  // pointer to an object such as a thread object
};


class CCortexThreadInfo  
{
public:
	CCortexThreadInfo();
	virtual ~CCortexThreadInfo();

	unsigned long m_dwThreadID;
	unsigned __int64 m_ui64Status;
	char* m_szStatusDescription;

	_timeb m_timebTick;
	char* m_szName;
	char* m_szDescription;

};


class CCortexDBSetting  
{
public:
	CCortexDBSetting();
	virtual ~CCortexDBSetting();

	char* m_psz_category_vc64;
	char* m_psz_category_display_name_vc64;
	char* m_psz_parameter_vc64;
	char* m_psz_value_vc256;
	char* m_psz_parameter_display_name_vc64;
	int   m_n_min_len;
	int   m_n_max_len;
	char* m_psz_pattern_vc64;
	char* m_psz_min_value_vc64;
	char* m_psz_max_value_vc64;
	int   m_n_compare_type; // tiny int ... ?
	int   m_n_sortorder;
	char* m_psz_description_vc256;
	bool  m_b_sys_only;

	unsigned long ulFlags;
		
};

class CCortexPlugIn  
{
public:
	CCortexPlugIn();
	virtual ~CCortexPlugIn();

	unsigned long  m_ulStatus;  // various states
	unsigned long  m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nID;  // the unique plugin ID within (assigned by db, indentity field)

	char* m_pszName;
	char* m_pszDesc;
	char* m_pszModule;
	char* m_pszStatus;
	char* m_pszChannelIds; // delimited string of channel IDs
	HINSTANCE				m_hinstDLL; // DLL load...
	LPFNDLLCTRL			m_lpfnDllCtrl; // pointer to function
	CRITICAL_SECTION m_critDllCtrl; 

	int m_nSettingsMod;
	int m_nLastSettingsMod;

	int m_nDeinitTimeoutMS;
	
	CCortexDBSetting** m_ppSettings;
	int m_nNumSettings;
	CRITICAL_SECTION m_critSettings; 

	void* m_pDataObj; // if needed

	int IsChannelAssociated(int nChannelID);

	CLicenseKey m_key;

	// util objects
	CBufferUtil m_bu;
	CSafeBufferUtil m_sbu;

};


class CCortexObject  
{
public:
	CCortexObject();
	virtual ~CCortexObject();

	// util objects
	CBufferUtil m_bu;
	CSafeBufferUtil m_sbu;
	CNetUtil m_net;  // network client only.  to send commands and keep connections open (uses m_socket)

	unsigned __int64 m_ui64Status;
	unsigned __int64 m_ui64Flags;
	unsigned short m_usType;

	char* m_pszName;
	char* m_pszDescription;

	int m_nPID; // if applicable

	// IP and ports
	char* m_pszHost;
	unsigned short m_usCommandPort;
	unsigned short m_usXMLPort;

	SOCKET m_socketCmd; // current comm socket for commands
	SOCKET m_socketXML; // current comm socket for commands

	// there is a negotiation to set the following from default values.
	unsigned long m_ulStatusIntervalMS;		// the interval, in milliseconds, at which we ask for status.
	unsigned long m_ulFailureIntervalMS;  // the interval, in milliseconds, at which we declare failure of object.

	// status received from object
	_timeb m_timebLastStatus; // the time of the last status received (parseable)
	_timeb m_timebLastTime;		// the last time received (object's local time)

	void* m_pStartupObj; // if applicable
	void* m_pDataObj; // if needed
};


class CCortexInternalData  
{
public:
	CCortexInternalData();
	virtual ~CCortexInternalData();

	CCortexItemBit** m_ppLogTypes;
	int m_nNumLogTypes;
	CRITICAL_SECTION m_critLogTypes;

	CCortexItemBit*** m_pppStatusTypes;
	int* m_pnNumStatusTypes;
	int m_nNumStatusTypeArrays;
	CRITICAL_SECTION* m_pcritStatusType;
	CRITICAL_SECTION m_critStatusTypeArray;

	CCortexItemBit** m_ppThreads;
	int m_nNumThreads;
	CRITICAL_SECTION m_critThreads;

// all the different object arrays that are necessary inside
	CCortexObject*** m_pppObj;
	int* m_pnNumObj;
	int m_nNumObjArrays;
	CRITICAL_SECTION* m_pcritObj;
	CRITICAL_SECTION m_critObjArray;

// all the different DLL arrays that are necessary inside
	CCortexPlugIn*** m_pppPlugIn;
	int* m_pnNumPlugIn;
	int m_nNumPlugInArrays;
	CRITICAL_SECTION* m_pcritPlugIn;
	CRITICAL_SECTION m_critPlugInArray;

	// util objects
	CBufferUtil m_bu;
	CSafeBufferUtil m_sbu;

	bool m_bProcessSuspended;
	CLicenseKey m_key;

};




#endif // !defined(AFX_CORTEXSHARED_H__BF3CE24A_470B_4F0F_8D0E_A7B654EECA03__INCLUDED_)
