; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCortexConsoleDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Cortex Console.h"

ClassCount=4
Class1=CCortexConsoleApp
Class2=CCortexConsoleDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_CORTEXCONSOLE_DIALOG
Resource2=IDR_MAINFRAME
Resource3=IDD_ABOUTBOX

[CLS:CCortexConsoleApp]
Type=0
HeaderFile=Cortex Console.h
ImplementationFile=Cortex Console.cpp
Filter=N

[CLS:CCortexConsoleDlg]
Type=0
HeaderFile=Cortex ConsoleDlg.h
ImplementationFile=Cortex ConsoleDlg.cpp
Filter=D

[CLS:CAboutDlg]
Type=0
HeaderFile=Cortex ConsoleDlg.h
ImplementationFile=Cortex ConsoleDlg.cpp
Filter=D

[DLG:IDD_CORTEXCONSOLE_DIALOG]
Type=1
Class=CCortexConsoleDlg
ControlCount=5
Control1=IDOK,button,1073807360
Control2=IDCANCEL,button,1073807360
Control3=IDC_LIST1,SysListView32,1350681613
Control4=IDC_LIST_SELECT,SysListView32,1342293005
Control5=IDC_LIST_SELECT2,SysListView32,1342293005

[DLG:IDD_ABOUTBOX]
Type=1
Class=?
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

