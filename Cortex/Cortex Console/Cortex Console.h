// Cortex Console.h : main header file for the CORTEX CONSOLE application
//

#if !defined(AFX_CORTEXCONSOLE_H__99D07B6E_6F82_4628_8E97_732A63DA916A__INCLUDED_)
#define AFX_CORTEXCONSOLE_H__99D07B6E_6F82_4628_8E97_732A63DA916A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include "../../../Common/IMG/BMP/CBmpUtil_MFC.h" 


/////////////////////////////////////////////////////////////////////////////
// CCortexConsoleApp:
// See Cortex Console.cpp for the implementation of this class
//

class CCortexConsoleApp : public CWinApp
{
public:
	CCortexConsoleApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexConsoleApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCortexConsoleApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONSOLE_H__99D07B6E_6F82_4628_8E97_732A63DA916A__INCLUDED_)
