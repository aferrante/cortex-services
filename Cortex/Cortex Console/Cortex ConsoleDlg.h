// Cortex ConsoleDlg.h : header file
//

#if !defined(AFX_CORTEXCONSOLEDLG_H__5BB59B7D_1798_4565_BEE1_D3318CC6E628__INCLUDED_)
#define AFX_CORTEXCONSOLEDLG_H__5BB59B7D_1798_4565_BEE1_D3318CC6E628__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
public: 
	HCURSOR m_hcurLink;

protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG


public:
  void OnLogo(UINT nFlags, CPoint point) ;


	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
// CCortexConsoleDlg dialog

class CCortexConsoleDlg : public CDialog
{
// Construction
public:
	CCortexConsoleDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CCortexConsoleDlg)
	enum { IDD = IDD_CORTEXCONSOLE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexConsoleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CCortexConsoleDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONSOLEDLG_H__5BB59B7D_1798_4565_BEE1_D3318CC6E628__INCLUDED_)


/*

	cortex console interface.
	needs left side menu and right side list box with info
	each menu item should have utils and such.

	Cortex
	  Start/Stop service
		get messages of type, date range
		set debug logging bytes


	Sentinel
	  Start/Stop service

	
	etc.

	Utilities
		timecalc
		TCP
		serial
		Harris API


*/





















