// Cortex ConsoleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Cortex Console.h"
#include "Cortex ConsoleDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
// CAbout requires MFC.

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
	m_hcurLink = NULL;

}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CAboutDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
  OnLogo(nFlags, point);

	CDialog::OnLButtonUp(nFlags, point);
}

BOOL CAboutDlg::OnInitDialog() 
{
  CDialog::OnInitDialog();

  
  CFont* pFont;
	CFont* pSetFont;
	CFont newFont;
  LOGFONT lfLogFont;
//  pFont = GetDlgItem(IDC_STATIC_URL)->GetFont();
  pFont = GetFont();
  COLORREF color;
  CDC* pdc;

  // set font to underline
	pSetFont =pFont;

  if(pFont->GetLogFont(&lfLogFont))
	{
		lfLogFont.lfUnderline = (BYTE) TRUE;
		if(newFont.CreateFontIndirect(&lfLogFont))
			pSetFont = &newFont;
	}

  // set color to blue...
  pdc = GetDlgItem(IDC_STATIC_URL)->GetDC( );
  color=RGB(0,0,255);
	if(pdc!=NULL)
	{
		CBmpUtil bmpu;
		HDC hdc= pdc->GetSafeHdc();
		((CStatic*)GetDlgItem(IDC_STATIC_URL))->SetBitmap(
			bmpu.TextOutBitmap(
				hdc, 
				"http://www.VideoDesignSoftware.com", 
				pSetFont, 
				color, 
				GetSysColor(COLOR_3DFACE), 
				0
				)
			);
		//center window
		CRect rcCtrl, rcWnd;
		GetClientRect(&rcWnd);
		GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcCtrl);
		ScreenToClient(&rcCtrl);
		GetDlgItem(IDC_STATIC_URL)->SetWindowPos(NULL, (rcWnd.Width()-rcCtrl.Width())/2, rcCtrl.top ,0,0,SWP_NOSIZE|SWP_NOZORDER);

	}
	else
	{
		GetDlgItem(IDC_STATICTEXT_URL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATICTEXT_URL)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_URL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_URL)->EnableWindow(FALSE);
	}

  CString szTemp;
  szTemp.Format("Build date: %s %s", __DATE__, __TIME__);
  GetDlgItem(IDC_STATIC_BUILD)->SetWindowText(szTemp);

#ifdef CX_CURRENT_VERSION
  szTemp.Format("Cortex Application\nVersion %s", CX_CURRENT_VERSION);
  GetDlgItem(IDC_STATICTEXT_TITLE)->SetWindowText(szTemp);
	szTemp.Format("About Cortex %s", CX_CURRENT_VERSION);
	SetWindowText(szTemp);
#endif

	m_hcurLink = AfxGetApp()->LoadCursor(IDC_CURSOR_LINK);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CAboutDlg::OnLogo(UINT nFlags, CPoint point)
{
	CRect rcLogo;
  GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcLogo);
  ScreenToClient(&rcLogo);
	if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
		(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
	{
    // launch browser
    HINSTANCE hi;
    hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignSoftware.com", NULL, NULL, SW_HIDE);
  }
	else
	{
		GetDlgItem(IDC_STATICTEXT_URL)->GetWindowRect(&rcLogo);
		ScreenToClient(&rcLogo);
		if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
			(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
		{
			// launch browser
			HINSTANCE hi;
			hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignSoftware.com", NULL, NULL, SW_HIDE);
		}
		else
		{
			GetDlgItem(IDC_STATIC_LOGO)->GetWindowRect(&rcLogo);
			ScreenToClient(&rcLogo);
			if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
				(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
			{
				// launch browser
				HINSTANCE hi;
				hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignSoftware.com", NULL, NULL, SW_HIDE);
			}
		}
	}
}


void CAboutDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	HCURSOR hCur = GetCursor();
	CRect rcLogo;
  GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcLogo);
  ScreenToClient(&rcLogo);
	if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
		(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
	{
		if(m_hcurLink!=hCur) SetCursor(m_hcurLink);
  }
	else
	{
		GetDlgItem(IDC_STATICTEXT_URL)->GetWindowRect(&rcLogo);
		ScreenToClient(&rcLogo);
		if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
			(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
		{
			if(m_hcurLink!=hCur) SetCursor(m_hcurLink);
		}
		else
		{
			GetDlgItem(IDC_STATIC_LOGO)->GetWindowRect(&rcLogo);
			ScreenToClient(&rcLogo);
			if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
				(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
			{
				if(m_hcurLink!=hCur) SetCursor(m_hcurLink);
			}
			else
			{
				// just set it back to the Arrow
			}
		}
	}
	
	CDialog::OnMouseMove(nFlags, point);
}


/////////////////////////////////////////////////////////////////////////////
// CCortexConsoleDlg dialog

CCortexConsoleDlg::CCortexConsoleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCortexConsoleDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCortexConsoleDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCortexConsoleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCortexConsoleDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCortexConsoleDlg, CDialog)
	//{{AFX_MSG_MAP(CCortexConsoleDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCortexConsoleDlg message handlers

BOOL CCortexConsoleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCortexConsoleDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCortexConsoleDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCortexConsoleDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}
