// CortexShared.cpp: implementation of the CCortexMessage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CortexShared.h"

#include <atlbase.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// CCortexMessage Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexMessage::CCortexMessage()
{
	int b=CX_XML_BUFFER_CORTEXBEGIN;
	while(b<CX_XML_BUFFER_COUNT)
	{
		m_pchResponse[b] = NULL;
		b++;
	}
	m_nTxStep =  CX_XML_BUFFER_CORTEXBEGIN;
	m_nFailure = CX_XML_ERR_SUCCESS;
	m_nType =    CX_XML_TYPE_UNK;
	m_pchType = NULL;
}

CCortexMessage::~CCortexMessage()
{
//	AfxMessageBox("foo");
	int b=CX_XML_BUFFER_CORTEXBEGIN;
	while(b<CX_XML_BUFFER_COUNT)
	{
		if(m_pchResponse[b])
		{
			try {free(m_pchResponse[b]);}
			catch(...) {}
		}
		b++;
	}
//	AfxMessageBox("foop");
	if(m_pchType)
	{
		try {free(m_pchType);}
		catch(...) {}
	}
//	AfxMessageBox("foox");
}

int CCortexMessage::FormatContent(int nBufferIndex, int nBufferMax, char* pchContent, ...)
{
	if((nBufferIndex>=CX_XML_BUFFER_CORTEXBEGIN)&&(nBufferIndex<CX_XML_BUFFER_COUNT)&&(pchContent))
	{
		char* pchMessage = (char*)malloc(nBufferMax+1);
		if(pchMessage)
		{
			va_list marker;
			// create the formatted output string
			va_start(marker, pchContent); // Initialize variable arguments.
#if _MSC_VER > 1200
			_vsnprintf_s(pchMessage, nBufferMax, pchContent, (va_list) marker);
#else
			_vsnprintf(pchMessage, nBufferMax, pchContent, (va_list) marker);
#endif // _MSC_VER > 1200
			va_end( marker );             // Reset variable arguments.

			if(m_pchResponse[nBufferIndex])	{	try { free(m_pchResponse[nBufferIndex]); } catch(...){}	}
			m_pchResponse[nBufferIndex] = (char*) malloc(strlen(pchMessage)+1);
			if(m_pchResponse[nBufferIndex])
			{
				strcpy(m_pchResponse[nBufferIndex], pchMessage);
				try { free(pchMessage); } catch(...){}
				return CX_SUCCESS;
			}
			else
			{
				try { free(pchMessage); } catch(...){}
			}
		}
	}
	return CX_ERROR;
}


int CCortexMessage::SetType(char* pchContent)
{
	if(pchContent)
	{
		if(m_pchType)	{	try { free(m_pchType); } catch(...){}	}
		m_pchType = (char*) malloc(strlen(pchContent)+1);
		if(m_pchType)
		{
			strcpy(m_pchType, pchContent);
			return CX_SUCCESS;
		}
	}
	return CX_ERROR;
}

int CCortexMessage::SetContent(int nBufferIndex, char* pchContent)
{
	if((nBufferIndex>=CX_XML_BUFFER_CORTEXBEGIN)&&(nBufferIndex<CX_XML_BUFFER_COUNT)&&(pchContent))
	{
		if(m_pchResponse[nBufferIndex])	{	try { free(m_pchResponse[nBufferIndex]); } catch(...){}	}
		m_pchResponse[nBufferIndex] = (char*) malloc(strlen(pchContent)+1);
		if(m_pchResponse[nBufferIndex])
		{
			strcpy(m_pchResponse[nBufferIndex], pchContent);
			return CX_SUCCESS;
		}
	}
	return CX_ERROR;
}

int CCortexMessage::ClearContent(int nBufferIndex)
{
	if((nBufferIndex>=CX_XML_BUFFER_CORTEXBEGIN)&&(nBufferIndex<CX_XML_BUFFER_COUNT))
	{
		if(m_pchResponse[nBufferIndex])	{	try { free(m_pchResponse[nBufferIndex]); } catch(...){}	}
		m_pchResponse[nBufferIndex] = NULL; // must assign NULL!
		return CX_SUCCESS;
	}
	return CX_ERROR;
}


char* CCortexMessage::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	char* pchText = NULL;
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					
					char* pchNode = W2A(_bstr_t(pValue->GetnodeValue()));
					if(pchNode)
					{
						pchText = (char*)malloc(strlen(pchNode)+1);
						if(pchText) strcpy(pchText, pchNode);
					}
				}
			}
		}
	}
	return pchText;
}

//////////////////////////////////////////////////////////////////////
// CCortexUtil Construction/Destruction
//////////////////////////////////////////////////////////////////////


CCortexUtil::CCortexUtil()
{
}

CCortexUtil::~CCortexUtil()
{
}

char* CCortexUtil::ConvertWideToA(void* pbstrIn)
{
	// this function expects pbstrIn to actually be the address of a _bstr_t
USES_CONVERSION;
	if(pbstrIn)
	{
		_bstr_t* pbstr = (_bstr_t*)pbstrIn;
		char* pch = W2A(*pbstr); // pch* goes out of scope and frees when this funtion returns
		if(pch)
		{
			char* pchReturn = (char*)malloc(strlen(pch)+1);
			{
				if(pchReturn)
				{
					strcpy(pchReturn, pch);
					return pchReturn;
				}
			}
		}
	}
	return NULL;
}


int CCortexUtil::ConvertWideToA(char* pchBuffer, unsigned long ulMaxBufLen, void* pbstrIn)
{
	int nReturn = CX_ERROR;
	char* pchTag = ConvertWideToA(pbstrIn);
	if(pchTag)
	{
		nReturn = CX_SUCCESS+((strlen(pchTag)>ulMaxBufLen)?1:0); // truncated
		strncpy(pchBuffer, pchTag, ulMaxBufLen);
		try{ free(pchTag); } catch(...){}
	}
	else 
	{
		strcpy(pchBuffer, "");
	}
	return nReturn;
}



//////////////////////////////////////////////////////////////////////
// CCortexItemBit Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexItemBit::CCortexItemBit()
{
	m_ui64Value = 0;
	m_szDescription = NULL;
	m_szName = NULL;
	m_pVoid = NULL;
}

CCortexItemBit::~CCortexItemBit()
{
	if(m_szName) free (m_szName);
	if(m_szDescription) free (m_szDescription);
	if(m_pVoid) delete m_szDescription;
}




//////////////////////////////////////////////////////////////////////
// CCortexThreadInfo Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexThreadInfo::CCortexThreadInfo()
{

	m_dwThreadID = 0xffffffff;
	__int64 m_ui64Status = 0;
	m_szStatusDescription = NULL;

	//_timeb m_timebTick;
	m_szName = NULL;
	m_szDescription = NULL;
}

CCortexThreadInfo::~CCortexThreadInfo()
{
	if(m_szStatusDescription) free (m_szStatusDescription);
	if(m_szName) free (m_szName);
	if(m_szDescription) free (m_szDescription);
}



//////////////////////////////////////////////////////////////////////
// CCortexDBSetting Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexDBSetting::CCortexDBSetting()
{
	m_psz_category_vc64 = NULL;
	m_psz_category_display_name_vc64 = NULL;
	m_psz_parameter_vc64 = NULL;
	m_psz_value_vc256 = NULL;
	m_psz_parameter_display_name_vc64 = NULL;
	m_n_min_len=0;
	m_n_max_len=-1;
	m_psz_pattern_vc64 = NULL;
	m_psz_min_value_vc64 = NULL;
	m_psz_max_value_vc64 = NULL;
	m_n_compare_type = -1;
	m_n_sortorder = -1;
	m_psz_description_vc256 = NULL;
	m_b_sys_only = false;

	ulFlags = 0;
}

CCortexDBSetting::~CCortexDBSetting()
{
	if(m_psz_category_vc64) try{ free(m_psz_category_vc64); } catch(...){}
	if(m_psz_category_display_name_vc64) try{ free(m_psz_category_display_name_vc64); } catch(...){}
	if(m_psz_parameter_vc64) try{ free(m_psz_parameter_vc64); } catch(...){}
	if(m_psz_value_vc256) try{ free(m_psz_value_vc256); } catch(...){}
	if(m_psz_parameter_display_name_vc64) try{ free(m_psz_parameter_display_name_vc64); } catch(...){}
	if(m_psz_pattern_vc64) try{ free(m_psz_pattern_vc64); } catch(...){}
	if(m_psz_min_value_vc64) try{ free(m_psz_min_value_vc64); } catch(...){}
	if(m_psz_max_value_vc64) try{ free(m_psz_max_value_vc64); } catch(...){}
	if(m_psz_description_vc256) try{ free(m_psz_description_vc256); } catch(...){}
}

//////////////////////////////////////////////////////////////////////
// CCortexPlugIn Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexPlugIn::CCortexPlugIn()
{
	m_ulStatus=0;  // various states
	m_ulFlags=0;   // various flags
	m_usType=0;

	m_nID=-1;  // the unique plugin ID within (assigned by db, indentity field)

	m_pszName=NULL;
	m_pszDesc=NULL;
	m_pszModule=NULL;
	m_pszStatus=NULL;
	m_pszChannelIds=NULL; // delimited string of channel IDs

	m_hinstDLL=NULL; // DLL load...
	m_lpfnDllCtrl=NULL; // pointer to function

	m_nSettingsMod=-1;
	m_nLastSettingsMod=-2;

	m_nDeinitTimeoutMS = -1;

	m_ppSettings=NULL;
	m_nNumSettings=0;
	InitializeCriticalSection(&m_critSettings); 

	// user-settable stuff:
	m_ppObj=NULL;
	m_nNumObj=0;
	InitializeCriticalSection(&m_critObj); 

	m_ppAuxObj=NULL;
	m_nNumAuxObj=0;
	InitializeCriticalSection(&m_critAuxObj); 

	InitializeCriticalSection(&m_critDllCtrl); 

}

CCortexPlugIn::~CCortexPlugIn()
{
	if(m_pszName) try{ free(m_pszName); } catch(...){}
	if(m_pszDesc)  try{ free(m_pszDesc); } catch(...){}
	if(m_pszModule)  try{ free(m_pszModule); } catch(...){}
	if(m_pszStatus)  try{ free(m_pszStatus); } catch(...){}
	if(m_pszChannelIds)  try{ free(m_pszChannelIds); } catch(...){}

	EnterCriticalSection(&m_critDllCtrl); 
	if(m_hinstDLL)
	{
		try
		{
			FreeLibrary(m_hinstDLL); 
		}
		catch(...)
		{
		}
		m_hinstDLL = NULL;
		m_lpfnDllCtrl = NULL;
	}
	LeaveCriticalSection(&m_critDllCtrl); 
	EnterCriticalSection(&m_critSettings); 
	if(m_ppObj)
	{
		int i=0;
		while(i<m_nNumSettings)
		{
			if(m_ppSettings[i]) try{ delete m_ppSettings[i]; } catch(...){} // delete objects, must use new to allocate
			i++;
		}
		try{delete [] m_ppSettings; } catch(...){}// delete array of pointers to objects, must use new to allocate
	}
	LeaveCriticalSection(&m_critSettings); 
	EnterCriticalSection(&m_critObj); 
	if(m_ppObj)
	{
		int i=0;
		while(i<m_nNumObj)
		{
			if(m_ppObj[i]) try{ delete m_ppObj[i]; } catch(...){} // delete objects, must use new to allocate
			i++;
		}
		try{delete [] m_ppObj; } catch(...){}// delete array of pointers to objects, must use new to allocate
	}
	LeaveCriticalSection(&m_critObj); 
	EnterCriticalSection(&m_critAuxObj); 
	if(m_ppAuxObj)
	{
		int i=0;
		while(i<m_nNumAuxObj)
		{
			if(m_ppAuxObj[i]) try{ delete m_ppAuxObj[i]; } catch(...){} // delete objects, must use new to allocate
			i++;
		}
		try{delete [] m_ppAuxObj; } catch(...){}// delete array of pointers to objects, must use new to allocate
	}
	LeaveCriticalSection(&m_critAuxObj); 

	DeleteCriticalSection(&m_critObj); 
	DeleteCriticalSection(&m_critAuxObj); 
	DeleteCriticalSection(&m_critSettings); 
	DeleteCriticalSection(&m_critDllCtrl); 
}


int CCortexPlugIn::IsChannelAssociated(int nChannelID)
{
	if(nChannelID>0)  // has to be a valid channel
	{
		if(m_pszChannelIds) // and there has to be data
		{
			unsigned long ulBufferLength = strlen(m_pszChannelIds); // really, there has to be data
			if(m_pszChannelIds)
			{
				CSafeBufferUtil sbu;
				char* pch = sbu.Token(m_pszChannelIds, ulBufferLength, " ,.:-|/\\;+");  // expect commas but really can be anything non numerical.  let's just use some punctuation.
				while(pch)
				{
					if(atoi(pch) == nChannelID)
					{
						return CX_SUCCESS;
					}
					else
					{
						pch = sbu.Token(NULL, NULL, " ,.:-|/\\;+");
					}
				}
			}
		}
	}
	return CX_ERROR;
}
